#pragma once

#include <codecvt>
#include <tlhelp32.h>
#include <UIAutomationClient.h>
#include "Common/ESDBasePlugin.h"

/// <summary>
/// Contains information about single process and associated windows
/// </summary>
struct ProcessWindowsInfo
{
	unsigned long process_id;
	std::vector<HWND> window_handles;
};

class SwitchToPlugin : public ESDBasePlugin
{
private:
	std::string process;
	std::string executable;
	std::string parameters;
	IUIAutomation* uiAutomation;

	bool equals_case_insensitive(const wchar_t* a, const wchar_t* b);
	static bool is_main_window(HWND handle);
	static BOOL CALLBACK enum_windows_callback(HWND handle, LPARAM lParam);
	std::vector<ProcessWindowsInfo> find_windows(std::vector<DWORD> process_ids);
	std::vector<DWORD> find_process_ids(const std::wstring& processName);
	bool bring_to_front(HWND hWnd);
	bool activate_main_window(const std::string& processName);
	void start_process(const std::string& processPath, const std::string& parameters);

public:
	
	SwitchToPlugin();
	virtual ~SwitchToPlugin();
	
	void KeyDownForAction(const std::string& inAction, 
		const std::string& inContext, 
		const json &inPayload, 
		const std::string& inDeviceID) override;
	void KeyUpForAction(const std::string& inAction, 
		const std::string& inContext, 
		const json &inPayload, 
		const std::string& inDeviceID) override;
	
	void WillAppearForAction(const std::string& inAction, 
		const std::string& inContext, 
		const json &inPayload, 
		const std::string& inDeviceID) override;
	void WillDisappearForAction(const std::string& inAction, 
		const std::string& inContext, 
		const json &inPayload, 
		const std::string& inDeviceID) override;
	
	void DeviceDidConnect(const std::string& inDeviceID, 
		const json &inDeviceInfo) override;
	void DeviceDidDisconnect(const std::string& inDeviceID) override;
	
	void SendToPlugin(const std::string& inAction, 
		const std::string& inContext, 
		const json &inPayload, 
		const std::string& inDeviceID) override;
};
