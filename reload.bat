set pluginName=switchto

if not exist "Release" (
mkdir Release
)

taskkill /IM StreamDeck.exe /F
timeout /t 1

del Release\pl.spooksoft.%pluginName%.streamDeckPlugin
xcopy /y Sources\Windows\x64\Release\*.exe Resources\pl.spooksoft.%pluginName%.sdPlugin\
DistributionTool.exe -b -i Resources\pl.spooksoft.%pluginName%.sdPlugin -o Release

rmdir /s /q "C:\Users\%USERNAME%\AppData\Roaming\Elgato\StreamDeck\Plugins\pl.spooksoft.%pluginName%.sdPlugin"
start "" "Release/pl.spooksoft.%pluginName%.streamDeckPlugin"

pause
